# Projet Web 2021 - Garrigues, Pilleyre

## Sommaire:

- I - [Introcution](#introduction)
  - 1 - [Objectifs](#objectifs)
- II - [Installation et lancements](#installation-et-lancements)
    - 1 - [Front-end](#front-end-application-mobile)
    - 2 - [Back-end](#back-end-serveur-web)
- III - [Tests](#tests)
- IV - [Backend API](#backend-api)
- V - [Cas d'utilisation](#cas-d'utilisation)
- VI [Choix Techniques](#choix-techniques)
  - 1 - [Les Web-services](#les-web-services-backend)
  - 2 - [La gestion des rôles](#la-gestion-des-rôles)
  - 3 - [L'architecture des deux applications](#larchitecture-des-deux-applications-back-end-et-front-end)
- VII - [Modèle de données](#modèle-de-données)
- VIII - [Liens](#liens)
- Before End - [La suite...](#la-suite-)
- End - [TO-DOs](#to-dos)

## Introduction
### Objectifs
L'idée principale et initiale de ce projet a été de créer une application qui se base sur l'API du site Twitch et propose à ses utilisateurs :
- d'accèder aux streamers et à leurs informations
- d'accèder aux clips
- de liker les clips qu'ils apprécient.

## Installation et lancements
Ce projet se décompose en deux parties et donc deux dépôts GitLab.
Le premier est celui de l'application mobile en elle-même, le "font-end" et l'autre est celui du serveur web, le "back-end".

### Front-End (application mobile)
**Après le `git clone` :**

```sh
$ npm i
```

*Note : L'installation des modules node peut, en cas d'erreur, être fixé par la suppression du répertoire `node_modules` (également de `package-lock.json` parfois) et par le lancement de `npm i` (ou `npm install`) à nouveau. Cette méthode n'est pas très propre mais fonctionnelle*

**Lancement de l'application :**
```sh
$ npx react-native start

$ npx react-native run-android # In another shell for example
```

**Connexion aux serveurs (heroku or localhost) :**
Le choix de l'accès à tel ou tel serveur se fait dans le fichier `helpers\serverAccessHelper.js`. Il suffit ensuite de choisir entre les différents URL proposés pour la constante `BACKEND` (un sera en commentaire l'autre dé-commenter).

***Pour le serveur localhost :***

Pour que la Connexion à ce serveur fonctionne et que les requêtes marche correctement il faudra, une fois l'application lancé dans l'émulateur, éxécuter la commande `adb reverse tcp:5000 tcp:5000` pour faire fonctionner la redirection entre l'adresse `10.0.2.2:5000` et `localhost:5000`.

*Note : Il est possible que lors de la Connexion via Twitch, le back-end retourne une erreur et que la Connexion ne se fasse pas correctement. Cette erreur est dûe à la redirection qui ne fait pas forcément passer tous les champs (comme les champs ajoutés et non-classique) dans la requête HTTP. Pour régler ce problème une solution est de relancer le serveur back-end (Ce problème n'a pas lieu d'être pour une Connexion sur le back-end Heroku).*

***Pour le serveur Heroku***

Pas de choses particulières à préciser.


### Back-End (serveur web)
Lien du dépôt : https://gitlab.com/alexpill/ensimag-2aa-web-project-backend

**Pour l'installation en localhost :**

```sh
$ git clone https://gitlab.ensimag.fr/pilleyra/projet-web-2021-backend.git <directory> # cloning the repo
$ cd <directory>
$ npm i # installing all the required node modules
```
**Lancement en localhost**
```sh
# When in the repo directory
$ npm start # node initdb.js && node index.js
```
*Note : la commande `npm start` a déjà renvoyé des erreurs. Si cela arrive il est toujours possible de lancer les deux commandes suivantes, qui feront exactement la même chose : `node initdb.js` et `node index.js`*

*Note 2 : De même l'installation des modules node peut, en cas d'erreur, être fixé par la suppression du répertoire `node_modules` (également de `package-lock.json` parfois) et par le lancement de `npm i` (ou `npm install`) à nouveau. Cette méthode n'est pas très propre mais fonctionnelle*

**Heroku :**

Le serveur est déployé à l'adresse suivante : https://young-plateau-14267.herokuapp.com

*Note : Le serveur heroku est censé être déployé sur la dernière version. Cependant un oubli de déploiement est toujours possible. N'hésitez pas nous contacter en cas de problèmes. Le bouton `deploy to heroku` devrait être ajouté prochainement *


## Tests
Le lancement tests `Detox` se font via la commande suivante : `detox-cli build && detox-cli test`

**Attention** : dans le fichier `.detoxrc.json` dans la commande `cd android && gradlew [...]`, on notera que `gradlew` n'est pas `./gradlew`. Ceci est voulu dans notre cas car nous développons sur un environnement Windows` mais il est possible que la commande ne soit pas reconnue comme il faut pour un environnement linux, auquel cas il faudra probablement faire la modification.

*Note: Si vous êtes sur windows, il est possible que `detox-cli` ne fonctionne pas correctement, une erreur étant renvoyée au lancement du programme. La solution, qui n'est pas forcément conseillé par `detox` est de lancer les commandes suivantes :*
```sh
# current directory is project root
# This is for Windows (that's why we use '\' instead of '/'...)
$ node node_modules\detox\local-cli\cli.js build
$ node node_modules\detox\local-cli\cli.js test
```
*De cette manière, on reproduit en partie ce que fait `detox-cli` et ainsi lancer les tests*.

## Backend API
Voici les différentes requêtes possibles pour appeler l'API du backend

### Pour la Connexion
- `/login` :
    - **Description** : Permet à l'utilisateur de récupérer un token de Connexion à partir d'un login et d'un mot de passe si l'utilisateur éxiste en BD.
    - **Méthode** : `POST`
    - **Paramètres** à fournir : `login` et `password` en **body** de la requête.
    - **Retour** : `token` de Connexion au back-end.
- `/oaut` et `oaut/authorize` :
    - **Description** : Permet de se Connexion avec son compte Twitch via OAuth2 et récupérer un token de Connexion du backend.
    - **Méthode** : *any (route utilisée par la Connexion OAuth2)*.
    - **Paramètres** à fournir: `redirect_url`, `client_id` et tous autres paramètres nécessaire à la Connexion OAuth2 *(dans le front-end, cette Connexion se fait via le paquet `react-native-app-auth`)*.
    - **Retour** : `token` de Connexion au back-end.

### Pour un utilisateur connecté
Toutes les requêtes suivantes et plus généralement les requêtes commençant par `api` nécessite un token de connexion du back-end valide.

- `/api/user` :
    - **Description** : Permet de connaître son nom d'utilisateur.
    - **Méthode** : `GET`
    - **Paramètres** : aucun paramètre
    - **Retour** : `username` qui contient le nom de l'utilisateur.
- `/api/isAdmin` :
    - **Description** : Permet de savoir si l'on est un admin ou non.
    - **Méthode** : `GET`
    - **Paramètres** : aucun paramètre
    - **Retour** : `role` qui contient un booléen à `true` si l'utilisateur est bien un admin et `false` sinon.
- `/api/clips/:clip_id/upvote` :
    - **Description** : Ajoute une entrée en BD avec le nom de l'utilisateur et le clip *liké*. Un utilisateur ne peut *liker* un clip qu'une seule fois.
    - **Méthode** : `POST`
    - **Paramètres** : `clip_id` qui est passé en paramètre dans l'URL de la requête.
    - **Retour** : Retourne le message `'upvoted'` en cas de réussite ou `'error upvoting'` sinon.
- `/api/clips/:clip_id/downvote` :
    - **Description** : Supprime l'entrée correspondante au clip et à l'utilisateur en BD. Retourne un message d'erreur si le couple n'est pas présent en DB.
    - **Méthode** : `POST`
    - **Paramètres** : `clip_id` qui est passé en paramètre dans l'URL de la requête.
    - **Retour** : Retourne le message `'downvoted'` en cas de succès et `'error downvoting'` sinon.
- `/api/clips/:clip_id/checkvoted` :
    - **Description** : Permet de savoir si un utilisateur a déjà *liké* un clip.
    - **Méthode** : `GET`
    - **Paramètres** : `clip_id` qui est passé en paramètre dans l'URL de la requête.
    - **Retour** : `voted` à `true` si déjà *liké* et `false` sinon.

### Pour l'Administrateur
Comme précédemment, il faut pour faire les requêtes suivante disposer d'un token de connexion du back-end valide et également que l'utilisateur soit un utilisateur en BD.

- `/api/admin/users/:username/:passwd` :
    - **Description** : Permet à un utilisateur administrateur d'ajouter un utilisateur en BD.
    - **Méthode** : `POST`
    - **Paramètres** : `username` et `passwd`, respectivement le nom de l'utilisateur et son mot de passe, qui sont des paramètres passé dans l'URL de la requête.
    - **Retour** : Un message confirmant l'ajout en BD, spécifiant que l'utilisateur existe déjà ou bien une erreur dans les autres cas.
- `/api/admin/users/:username` :
    - **Description** : Permet à un utilisateur administrateur de supprimer un utilisateur en BD.
    - **Méthode** : `DELETE`
    - **Paramètres** : `username`, le nom d'utilisateur à supprimer, qui est passé en paramètre dans l'URL de la requête.
    - **Retour** : Un message confirmant la suppression depuis la BD, spécifiant que l'utilisateur n'existe pas ou bien une erreur dans les autres cas.
- `/api/admin/users` :
    - **Description** : Permet à un utilisateur administrateur de récupérer la liste de tous les utilisateurs en DB dans la table `users`.
    - **Méthode** : `GET`
    - **Paramètres** : aucun paramètre
    - **Retour** : Retourne la liste des utilisateurs présent dans la table `users` en BD, dans le body de la réponse.

### Pour un utilisateur non connecté (visiteur)
- `/public_api/proxy` :
    - **Description** : Permet de faire une requête vers l'API de Twitch en passant par le proxy fournit par le back-end
    - **Méthode** : Any, dépend de la requête vers l'API Twitch (voir https://dev.twitch.tv/docs/api/)
    - **Paramètres** : Une requête classique vers l'API Twitch ressemble à ceci : `https://api.twitch.tv/helix/<fin_de_la_requête>`. Ici il n'est nécessaire d'ajouter à la fin de la requête vers le back-end que la partie `<fin_de_la_requête>` donc `/public_api/proxy/<fin_de_la_requête>`.
    - **Retour** : voir la documentation de l'API de Twitch (voir https://dev.twitch.tv/docs/api/).
- `/public_api/clips/:clip_id/score` :
    - **Description** : Permet de savoir le nombre de like sur un clip donné.
    - **Méthode** : `GET`
    - **Paramètres** : `clip_id` qui est passé en paramètre dans l'URL de la requête.
    - **Retour** : Retourne le score c'est à dire le nombre de like sur un clip ou bien un message pour dire que le clip n'existe pas (qu'il a donc un score de 0 et aucune occurrence en BD).

## Cas d'utilisation
![use case](./markdown_pics/usecase.svg)

Voici ci-dessus un diagramme de cas-utilisation montrant les différentes intéractions possibles pour les différents utilisateurs
.

Scénarios d'usage:
- Je suis un visiteur :
  - Je peux : cliquer sur le bouton `Continuer sans se connecter` puis naviguer dans l'application, chercher des clips, chercher des streamers, cliquer sur un streamer pour voir ses détails, cliquer sur un clip pour le regarder et voir le nombre de likes.
  - Je ne peux pas : liker un clip

- Je suis un utilisateur connecté :
  - Je peux : me connecter en cliquant sur `Se connecter` après avoir spécifier mon mot de passe et mon nom d'utilisateur si je suis un utilisateur enregistré en base de donnée ou bien me connecter via Twitch pour accéder à l'application. Ensuite je peux faire tout ce que le visiteur fait et également ce qu'il ne peut pas.
  - Je ne peux pas : gérer les utilisateurs.

- Je suis un administrateur :
  - Je peux : faire tout ce qu'un utilisateur connecté peut faire ainsi que ce qu'il ne peut pas faire
  - Je ne peux pas ajouter un role à un utilisateur.

## Choix techniques
Avant toute chose, nous tenons à dire qu'une partie que le back-end est une évolution de la correction qui été mise à disposition. Plus particulièrement, nous avons utiliser la branche à propos du proxy OAuth comme point de départ.
### Les Web-services (backend)
Les différents services fournis par le back-end se présentent sous la forme de *services*. Ainsi il existe plusieurs services :
- `adminApi` : permets de gérer les actions et opérations pour un admin (ajouter/supprimer utilisateur, voir la liste des utilisateurs). Ce service fournit les méthodes suivantes :
  - `authorizeAdmin1` : fonction middleware qui permets de vérifier si l'utilisateur est bien un admin.
  - `checkRequest` : fonction middleware permettant de vérifier que la requête est bien valide.
  - `listUsers` : fonction permettant de récupérer la liste des utilisateurs présents dans la table `users` en BD.
  - `addUser` : fonction permettant d'ajouter un utilisateur en BD en fournissant son `username` et son `passwd`.
  - `delUser` : fonction permettant de supprimer un utilisateur en BD en fournissant son `username`.
  - `isAdmin` : fonction renvoyant un booléen pour savoir sur un utilisateur est un admin *(comme pour le middle ware mais est accessible par tout utilisateur connecté)*.
- `twitchApi` : permets de gérer tout ce qui touche à la gestion des clips et également d'accéder à l'API Twitch via le proxy fournit par le serveur. Ce service fournit les méthodes suivantes :
  - `init` : fonction permettant d'initialiser le service.
  - `getClipScore` : fonction permettant de récupérer le nombre de likes qu'un clip a reçu en lisant en BD le nombre de ligne ou le clip apparaît.
  - `checkVoted` : fonction permettant de savoir si un utilisateur a déjà voté pour un clip *(un utilisateur ne peur voter que une seule fois par clip)*.
  - `upvoteClip` : fonction permettant à un utilisateur de voter/liker un clip. Ajoute un couple `clip_id/username` en BD.
  - `downvoteClip` : fonction permettant à un utilisateur de *dé-liker* un clip. Supprime une ligne dans la BD.
  - `publicApi` : fonction permettant d'accèder à l'API Twitch via le back-end *(proxy).
- Différents autre services fournit par le projet de base pour faciliter la connexion et l'authentification via OAuth2.

Le relai vers chacun des services se fait par l'utilisation du `router` de `express`. Les différentes routes sont présentes dans le dossier `routes` du backend :

- `api` : dirige les requêtes soit vers `api_admin` pour celles en lien avec les administrateur ou vers `api_clips`.
- `public_api` : concerne les requêtes publiques
- `oauth` : pour les requêtes en lien avec l'authentification
### La gestion des rôles
Il y a dans cette application plusieurs rôles possibles pour les utilisateurs :
- **visiteur** : utilisateur non connecté, dans une forme de *read-only*
- **utilisateur connecté** : utilisateur qui s'est connecté via l'authentification OAuth2 de Twitch ou bien via le back-end *(présent en BD soit dans la table `proxytwitch` soit dans la table `users`)
- **administrateur** : utilisateur connecté, pour lequel le rôle à été ajouté en dur dans la BD.

*Note : voir le use-case pour des détails sur les actions possibles par tel ou tel utilisateur.*

Le visiteur ne fait qu'utiliser les requêtes qui ne nécessitent pas d'authentification. Il n'y a donc pas vraiment de gestion particulière.

L'utilisateur connecté est soit présent en BD dans la table `users` soit dans la table `proxytwitch`. Deux cas sont donc possibles :
- Dans le premier cas il a été ajouté par un administrateur ou bien a été ajouté en dur directement via un accès à la BD. La vérification de sa connexion, dont notamment la vérification de son token de connexion se fait côté back-end par l'utilisation d'une fonction middleware qui va vérifier que le token est bien un token provenant du serveur et non pas d'un autre service *(tout cela se fait via l'utilisation du module **jws**)*. La fonction vérifie également que le token est bien celui de l'utilisateur et pas celui d'un autre.
- Dans le second cas, l'utilisateur se connecte en OAuth2 via twitch et par un « jeu » de redirection un token de connexion de Twitch est envoyé et stocké sur le back-end. À son tour le back-end génère un token de connexion pour l'utilisateur qui vient de se connecter à Twitch et stocke cet utilisateur dans la BD dans la table `proxytwitch`. Cette méthode de connexion ne nécessite pas d'intervention côté back-end (pas besoin d'administrateur ou d'insertion directe dans la BD), elle est donc à priviligié pour un nouvel utilisateur.

L'administrateur quant à lui est un utilisateur soit présent dans la table `users` soit dans la table `proxytwitch` mais est avant tout un utilisateur présent dans la table `roles` avec l'attribut `role` ayant pour valeur `admin`. L'insertion d'un role ne se fait que via un accès direct à la base de données. Un utilisateur créé par un administrateur ne peut donc pas directement être un administrateur à son tour. La vérification d'un administrateur se fait via l'utilisation d'une fonction middleware qui cherche en BD la ligne dans la table `roles` correspondant à l'utilisateur et vérifie que le rôle attribué à l'utilisateur est bien `admin`.

### L'architecture des deux applications (back-end et front-end)
**Front-end** :
- un dossier `helpers` qui contient des fichiers fournissant des fonctions ou des constantes pour faciliter, notamment, la connexion au back-end.
- un dossier `views` qui contient les différentes vues de l'application.
- un fichier `App.js` qui contient le point de départ de l'application et le fichier `index.js` qui initialise l'application.
- les dossiers classiques `android` et `ios` contenant les soruces générées par `react-native`. Le dossier `android` a par ailleurs été modifié pour faire fonctionner `detox`.
- un dossier `e2e` qui contient des tests `End-to-End` pour l'interface de l'application.
- un dossier `markdown_pics` qui contient des éléments pour le `README`.
- D'autres fichiers comme `.gitignore` ou `.eslintignore` facilitant la configuration du projet.


**Back-end** :
L'architecture du back-end se base sur la correction proposée. Le projet se découpe en plusieurs fichiers et répertoires de la sorte *(on se base par rapport à la racine du projet)* :
- un dossier `routes` qui contient toutes les routes permettant la redirection des requêtes vers la bonne fonction pouvant les prendre chacune en charge
- un dossier `services` qui contient différents fichiers qui contiennent les fonctions vers lesquelles sont redirigées les requêtes.
- un fichier `app.js` qui contient la base de l'application
- un fichier `initdb.js` qui est un script permettant comme son nom l'indique d'initialiser la base de données.
- un fichier `index.js` qui initialise l'application et permet le lancement de cette dernière.
- un dossier `__tests__` qui contient les tests `jest` pour les différents appels API.
- d'autre fichiers tel `.gitignore`, `Procfile`, `db[.test].sqlite` *(lorsque initialisés)* et autre permettant de gérer la configuration du projet.

## Modèle de données
L'ensemble des données du front-end est présent et stocké au sein de chaque vue, en utilisant par exemple les `useStates`.
Certaines données tel le token de connexion sont fournies par les `helpers` sous forme d'appel de fonction *(pour le token)* ou sous forme de constantes *(pour l'url du back-end par exemple)*.

Pour ce qui est des données côtés back-end, elles sont stockées dans la base de donnée dans les tables suivantes :
- `users` : contient la liste des utilisateurs classique avec leur mot de passe
- `proxytwitch` : contient la liste des utilisateurs connectés via OAuth2 sur Twitch
- `clips_score` : contient des couples `username/clip_id` permettant de référencer les votes des utilisateurs connectés pour les clips.
- `roles` : contient des couples `username/role` permettant de référencer les roles d'utilisateurs et particulièrement les utilisateurs administrateur.
- `oauth` : facilite la connexion d'un utilisateur.

Des fonctions assurent l'accès à la BD et sa gestion, notamment dans le dossier `services`.
## La suite ...
Il reste beaucoup de choses à faire dans projet notamment :
- Il faudrait hasher les mots de passe à la fois côté back-end et également lors de l'envoie via requête pour la connexion, pour rajouter de la sécurité. De manière générale il serait intéressant de prendre plus de temps pour gérer toute la partie sécurité de l'application qui est sûrement très vulnérable en l'état.
- Dans le même registre, il faudrait que tous les appels vers l'API Twitch qui se font depuis le front-end via le proxy du back-end deviennent des appels vers l'API du back-end. En effet en l'état il est en théorie possible de faire n'importe quel appel vers l'API Twitch depuis le front-end mais également via un navigateur ou outil de requête *(comme `curl`)*. Il serait donc intéressant de : implémenter un moyen de vérifier que les requêtes viennes bien de nombre application front-end et également de limiter l'ensembles des requêtes possibles pour se limiter à celles utilisées.
- Concernant la base de donnée, il serait probablement préférable de ne pas utiliser une tables `roles` mais d'ajouter une colonne à la table `users` pour attribuer un role à chaque utilisateur de cette table. De plus il serait intéressant de rassembler la table `users` et `proxytwitch` au sein d'une seule et même table. En effet, initialement la table `proxytwitch` permettait de stocker les tokens d'accès à l'API des utilisateurs connectés via Twitch mais ces tokens n'était plus nécessaire dans l'état actuel, le fait d'avoir les utilisateurs dans deux tables différentes n'est pas intéressant.
- Dans le back-end, les fonctions d'utilisation et d'accès à la base de donnée sont redéfinies plusieurs fois, il faudrait donc les rassembler au sein d'un même fichier.
- Actuellement un utilisateur administrateur peut se supprimer lui-même, il faudrait rajouter un moyen d'éviter cela.
- Utiliser des mock pour les test e2e avec `detox` pour éviter d'avoir des informations qui ne seront plus d'actualités sur Twitch au bon d'un moment...
- Il faudrait une fois l'amélioration de la BD effectué, faire en sorte qu'un rôle soit spécifiable par un administrateur lors de la création d'un utilisateur, ainsi que le rôle d'un utilisateur soit modifiable. On pourrait notamment imaginer qu'un utilisateur élu comme administrateur par un autre administrateur ne pourrait pas supprimer ce dernier. Il y aurait donc une hiérarchie parmi les administrateurs.
- Il faudrait sauvegarder l'état de l'application pour qu'une fois l'application quittée et relancée, l'utilisateur connecté n'est pas besoin de se reconnecter.
- Il faudrait fix et améliorer la CI/CD...
- Embellir l'interface et intégrer des concept d'expérience utilisateur et d'ergonomie et d'accessibilité...
- Améliorer le système de log et de debug
- Nettoyer et factoriser le code
- Ajouter un logo

*Note : certains éléments évoqués ci-dessus sont parfois présents dans la liste des To-do.*

## Liens
Lien pour une apk de l'application : https://drive.google.com/file/d/1bnAY-E4s8h_HCQD4hP2smugpgbCW5C_j/view?usp=sharing

Lien pour le screencast *(sans commentaires)* : https://drive.google.com/file/d/1NIVwoeq0O2NGNOZAzHr3VZnFyXfc7fyE/view?usp=sharing
## TO-DOs
Ce qu'il reste à faire :
- [ ] Add the deploy to heroku button
- [ ] Check why error when accessing clip as visitor (Json parse error in logs). Do not affect the application
- [ ] Use hash password in the database of the backend server
- [ ] Mocking for Detox test instead of using application

Pour le rendu :
- [x] ensemble du code pour une exécution de l'application web *(voir back-end)*
- [x] des fichiers permettant l'initialisation de la base de donnée utilisée pour avoir une application fonctionnelle *(voir back-end)*
- [x] une documentation pour l'installation et de test *(above)*
- [x] l'ensemble des jeux de tests back-end *(voir back-end)*
- [x] l'ensemble des jeux de tests front-end *(above, dossier `e2e`)*
- [ ] Optionnel : les élements d'intégration continue *(présent mais non fonctionnel,)* *(UPDATE : les tests unitaires sont lancés dans le back-end, le front-end ne marche toujours pas)*
- [x] Le fichier Readme.md avec:
  - [x] Les objectifs du projet
  - [x] Les cas d'usage
  - [x] Les différents scénarii d'usage (UML)
  - [x] Le modèle de donnée utilisé (diagramme de classe ?) *(pas d'uml...)*
  - [x] La documentation API côté back-end *(présente dans ce fichier README et non côté back)*
  - [x] Les choix techniques faits :
    - [x] Webservice utilisé et comment (dans quel contrôleur, quelle méthode)
    - [x] Décrire comment la gestion des rôles a été mise en place
    - [x] Décrire l'architecture de l'application (notamment sur l'organisation et le découpage de vos fichiers)
  - [x] Un lien vers screencast montrant l'application en action illustrant les usages principaux. Durée maxi 3 min *(voir la section 'Liens'... ScreenCast sans commentaires)*.
