import React, {useState} from 'react';
import {TextInput, View, StyleSheet} from 'react-native';
import MyButton from './MyButton';
const LoginForm = props => {
  const [login, setlogin] = useState('alzebor');
  const [password, setPassword] = useState('123456');

  return (
    <View>
      <TextInput
        style={styles.input}
        onChangeText={setlogin}
        value={login}
        placeholder="Login"
      />
      <TextInput
        style={styles.input}
        secureTextEntry={true}
        onChangeText={setPassword}
        value={password}
        placeholder="Password"
      />
      <MyButton
        title="Se connecter"
        onPress={() => props.onConnect(login, password)}
        // onPress={console.log()}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {flex: 1},
  input: {height: 40, margin: 12, borderWidth: 1},
});
export default LoginForm;
