import React from 'react';
import {Text, View, StyleSheet, TouchableHighlight} from 'react-native';
const MyButton = props => {
  return (
    <TouchableHighlight onPress={props.onPress}>
      <View style={styles.button}>
        <Text style={styles.buttonText}>{props.title}</Text>
      </View>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  buttonText: {
    color: 'white',
    fontSize: 20,
  },
  button: {
    alignItems: 'center',
    padding: 10,
    margin: 10,
    backgroundColor: 'lightgreen',
    borderRadius: 10,
    borderColor: 'blue',
    borderWidth: 3,
  },
});

export default MyButton;
