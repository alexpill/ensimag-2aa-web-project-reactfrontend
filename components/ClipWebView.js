import React from 'react';
import {StyleSheet} from 'react-native';
import {WebView} from 'react-native-webview';

const ClipWebView = props => {
  return (
    <WebView
      style={styles.WebViewStyle}
      source={{uri: props.urlClip + '&parent=AwesomeProject'}}
      javaScriptEnabled={true}
      domStorageEnabled={true}
    />
  );
};
const styles = StyleSheet.create({
  WebViewStyle: {
    margin: 20,
    flex: 0,
    height: 200,
  },
});
export default ClipWebView;
