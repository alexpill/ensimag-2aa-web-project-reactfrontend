import React from 'react';
import {View, StyleSheet, TouchableHighlight, Image} from 'react-native';
import {Card} from 'galio-framework';

const StreamerThumbnail = ({name, title, stream, thumbnail, onPress}) => {
  return (
    <View>
      <TouchableHighlight onPress={onPress}>
        <Card
          flex
          borderless
          title={title}
          caption={name}
          avatar={thumbnail}
          image={stream.replace('{width}', '1600').replace('{height}', '900')}
        />
      </TouchableHighlight>
    </View>
  );
};
export default StreamerThumbnail;
