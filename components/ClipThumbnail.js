import React from 'react';
import {View, TouchableHighlight} from 'react-native';
import {Card, Text} from 'galio-framework';

const ClipThumbnail = ({
  clip,
  streamerName,
  title,
  gameName,
  thumbnail,
  onPress,
}) => {
  return (
    <View>
      <TouchableHighlight onPress={onPress}>
        <Card
          flex
          borderless
          title={title}
          caption={streamerName}
          avatar={thumbnail}
          image={clip}
        />
      </TouchableHighlight>
      <Text p>{gameName}</Text>
    </View>
  );
};
export default ClipThumbnail;
