import React, {useState, useEffect} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {getToken} from '../helpers/tokenHelper';
import {Text, Switch} from 'galio-framework';

import {readApi} from '../helpers/apiHelper';

const StreamerDetail = ({navigation, route}) => {
  const {id, thumbnail, online} = route.params;
  async function getStreamer() {
    let query;
    if (online) {
      // query = `https://api.twitch.tv/helix/streams?user_id=${id}`;
      query = `/streams?user_id=${id}`;
    } else {
      // query = `https://api.twitch.tv/helix/channels?broadcaster_id=${id}`;
      query = `/channels?broadcaster_id=${id}`;
      console.log(id);
    }
    // fetch(query, {
    //   method: 'GET',
    //   headers: {
    //     'client-id': '4603kt3cahqmntrfyxd8y3g7liasqv',
    //     Authorization: `Bearer ${await getToken()}`,
    //   },
    // })
    readApi(query, 'GET')
      .then(response => response.json())
      .then(res => {
        let data = res.data[0];
        setTitle(data.title);
        setGameName(data.game_name);
        if (online) {
          setName(data.user_name);
          setViewersCount(data.viewer_count);
          setIsMature(data.is_mature);
        } else {
          setName(data.broadcaster_name);
        }
      })
      .catch(error => console.log(error));
  }

  const [name, setName] = useState('');
  const [title, setTitle] = useState('');
  const [gameName, setGameName] = useState('');
  const [viewersCount, setViewersCount] = useState(0);
  const [isMature, setIsMature] = useState(false);

  useEffect(() => {
    getStreamer();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View>
      <View style={styles.title}>
        <Image source={{uri: thumbnail}} style={{width: 300, height: 200}} />
        <Text h3 bold>
          {name}
        </Text>
        <Text p style={styles.textCentered}>
          {title}
        </Text>
      </View>
      <Text p style={styles.text}>
        Activité / Jeu : {gameName}
      </Text>
      <Text p color="green" style={styles.text}>
        En live :
        <Switch
          value={online}
          disabled
          onChange={() => {}}
          style={styles.Switch}
        />
      </Text>
      {online ? (
        <View>
          <Text p color="red" style={styles.text}>
            Nombre de viewers : {viewersCount}
          </Text>
          <Text p style={styles.text}>
            Stream avec du contenu sensible : {isMature ? 'Oui' : 'Non'}
          </Text>
        </View>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    alignItems: 'center',
  },
  switch: {
    paddingLeft: '10px',
  },
  textCentered: {
    textAlign: 'center',
    padding: 10,
  },
  text: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 5,
    paddingBottom: 5,
  },
});

export default StreamerDetail;
