import React, {useState, useEffect, useRef} from 'react';
import {
  RefreshControl,
  StyleSheet,
  View,
  ActivityIndicator,
  FlatList,
  Image,
  TouchableHighlight,
} from 'react-native';
import Item from '../components/ClipThumbnail';
import {getToken} from '../helpers/tokenHelper';
import {Picker} from '@react-native-picker/picker';
import {theme, Input, Button, Text} from 'galio-framework';
import Icon from 'react-native-vector-icons/AntDesign';

import {readApi} from '../helpers/apiHelper';

const ClipsScreen = ({navigation}) => {
  async function getGames() {
    isLoading(true);
    const token = await getToken();
    // fetch('https://api.twitch.tv/helix/games/top?first=100', {
    //   method: 'GET',
    //   headers: {
    //     'client-id': '4603kt3cahqmntrfyxd8y3g7liasqv',
    //     Authorization: `Bearer ${token}`,
    //   },
    // })
    readApi('/games/top?first=100', 'GET')
      .then(response => response.json())
      .then(data => {
        let list = [];
        data.data.forEach(g => {
          list.push({
            id: g.id,
            name: g.name,
            url: g.box_art_url
              .replace('{width}', '52')
              .replace('{height}', '72'),
          });
        });
        setGames(list);
        isLoading(false);
      })
      .catch(error => {
        isLoading(false);
        console.log(error);
      });
  }

  async function getClips() {
    isLoading(true);
    if (criteria === 'streamer') {
      const token = await getToken();
      // fetch(
      //   `https://api.twitch.tv/helix/search/channels?query=${search}&first=1`,
      //   {
      //     method: 'GET',
      //     headers: {
      //       'client-id': '4603kt3cahqmntrfyxd8y3g7liasqv',
      //       Authorization: `Bearer ${token}`,
      //     },
      //   },
      // )
      readApi(`/search/channels?query=${search}&first=1`, 'GET')
        .then(response => response.json())
        .then(data => {
          getClipsBySearch(data.data[0].id);
        })
        .catch(error => {
          isLoading(false);
          isRefreshing(false);
          console.log(error);
        });
    } else {
      getClipsBySearch(search);
    }
  }

  async function getClipsBySearch(searchCriteria) {
    isError(false);
    // let query = 'https://api.twitch.tv/helix/clips';
    let query = '/clips';
    switch (criteria) {
      case 'clipId':
        query += '?id=';
        break;
      case 'streamer':
        query += '?broadcaster_id=';
        break;
      case 'game':
        query += '?game_id=';
        break;
      default:
        isLoading(false);
        isRefreshing(false);
        console.log('Critère de recherche de clips introuvable.');
        break;
    }
    query += searchCriteria;
    if (pagination !== '') {
      query += `&after=${pagination}&first=10`;
    }
    const token = await getToken();
    // fetch(query, {
    //   method: 'GET',
    //   headers: {
    //     'client-id': '4603kt3cahqmntrfyxd8y3g7liasqv',
    //     Authorization: `Bearer ${token}`,
    //   },
    // })
    readApi(query, 'GET')
      .then(response => response.json())
      .then(data => {
        if (data.error !== undefined) {
          isError(true);
          isLoading(false);
          isRefreshing(false);
          return;
        }
        let list = [];
        if (pagination !== '') {
          list = [...DATA];
        }
        setPagination(
          data.pagination === undefined ? '' : data.pagination.cursor,
        );
        const promises = data.data.map(s =>
          // fetch(
          //   `https://api.twitch.tv/helix/search/channels?query=${s.broadcaster_name}&first=1`,
          //   {
          //     method: 'GET',
          //     headers: {
          //       'client-id': '4603kt3cahqmntrfyxd8y3g7liasqv',
          //       Authorization: `Bearer ${token}`,
          //     },
          //   },
          // ),
          readApi(
            `/search/channels?query=${s.broadcaster_name}&first=1`,
            'GET',
          ),
        );
        Promise.all(promises).then(values => {
          const channelsProm = values.map(v => v.json());
          Promise.all(channelsProm).then(channels => {
            data.data.forEach(async function (s) {
              let gameName = '';
              if (games.filter(g => g.id === s.game_id)[0] !== undefined) {
                gameName = games.filter(g => g.id === s.game_id)[0].name;
              } else {
                gameName = (
                  await (
                    await readApi(`/games?id=${s.game_id}&first=1`, 'GET')
                  ).json()
                ).data[0].name;
              }
              list.push({
                id: s.id,
                clipTb: s.thumbnail_url,
                streamerName: s.broadcaster_name,
                title: s.title,
                gameName: gameName,
                clipUrl: s.embed_url,
                thumbnail: channels.filter(
                  c => c.data[0].id === s.broadcaster_id,
                )[0].data[0].thumbnail_url,
              });
            });
            setData(list);
            isLoading(false);
            isRefreshing(false);
          });
        });
      })
      .catch(error => {
        isLoading(false);
        isRefreshing(false);
        console.log(error);
      });
  }

  function backToGames() {
    setPagination('');
    setSearch('');
    setCriteria('streamer');
    isLaunchingSearch(true);
  }

  const renderGame = ({item}) => {
    return (
      <TouchableHighlight
        style={styles.game}
        onPress={() => {
          setCriteria('game');
          setSearch(item.id);
          isLaunchingSearch(true);
        }}>
        <View style={styles.gameView}>
          <Image source={{uri: item.url}} style={{width: 52, height: 72}} />
          <Text p style={styles.gameText}>
            {item.name}
          </Text>
        </View>
      </TouchableHighlight>
    );
  };

  const renderItem = ({item}) => (
    <Item
      id={item.id}
      clip={item.clipTb}
      streamerName={item.streamerName}
      title={item.title}
      gameName={item.gameName}
      thumbnail={item.thumbnail}
      onPress={() =>
        navigation.navigate('ClipDetail', {url: item.clipUrl, id: item.id})
      }
    />
  );

  const renderFooter = () => {
    return (
      <View style={styles.indicator}>
        <ActivityIndicator size="large" color="black" />
      </View>
    );
  };

  const onRefresh = () => {
    setPagination('');
    isRefreshing(true);
  };

  const [criteria, setCriteria] = useState('streamer');
  const [DATA, setData] = useState([]);
  const [loading, isLoading] = useState(false);
  const [pagination, setPagination] = useState('');
  const [refreshing, isRefreshing] = useState(false);
  const [search, setSearch] = useState('');
  const [games, setGames] = useState([]);
  const [launchSearch, isLaunchingSearch] = useState(true);
  const [error, isError] = useState(false);
  const [TORefresh, isTOrefreshing] = useState(false);
  let timeoutInput = null;

  useEffect(() => {
    if (refreshing) {
      getClips();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [refreshing]);

  useEffect(() => {
    if (launchSearch) {
      console.log('launching search');
      if (search === '') {
        setData([]);
        getGames();
      } else {
        getClips();
      }
      isLaunchingSearch(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [launchSearch]);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button
          color="#ff7615"
          style={{width: 40, height: 40, marginRight: 20}}
          onPress={() => navigation.navigate('AddClip')}>
          <Icon name="pluscircleo" color="white" size={25} />
        </Button>
      ),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.Container}>
      <Picker
        selectedValue={criteria}
        onValueChange={(itemValue, itemIndex) => {
          setPagination('');
          setCriteria(itemValue);
        }}>
        <Picker.Item label="Id" value="clipId" />
        <Picker.Item label="Streamer" value="streamer" />
        <Picker.Item label="Jeu" value="game" />
      </Picker>
      <Input
        placeholder="Recherche..."
        color={theme.COLORS.INFO}
        style={{borderColor: theme.COLORS.INFO}}
        placeholderTextColor={theme.COLORS.INFO}
        onChangeText={text => {
          setSearch(text);
          clearTimeout(timeoutInput);
          if (!TORefresh) {
            timeoutInput = setTimeout(() => {
              setPagination('');
              isLaunchingSearch(true);
              isTOrefreshing(false);
            }, 500);
          }
          isTOrefreshing(true);
        }}
        value={search}
      />
      {loading === true && pagination === '' ? (
        <View style={styles.indicator}>
          <ActivityIndicator size="large" color="black" />
        </View>
      ) : (
        <View style={styles.Container}>
          {search === '' ? (
            <FlatList
              key={'G'}
              data={games}
              renderItem={renderGame}
              numColumns="2"
              keyExtractor={item => item.id}
            />
          ) : (
            <View>
              <Button
                onPress={backToGames}
                color="warning"
                style={{width: 150, height: 40}}>
                Retour aux jeux
              </Button>
              {error === false ? (
                <FlatList
                  key={'C'}
                  data={DATA}
                  renderItem={renderItem}
                  keyExtractor={item => item.id}
                  onEndReachedThreshold={0.4}
                  onEndReached={getClips.bind(this)}
                  ListFooterComponent={renderFooter.bind(this)}
                  refreshControl={
                    <RefreshControl
                      refreshing={refreshing}
                      onRefresh={onRefresh.bind(this)}
                    />
                  }
                />
              ) : (
                <Text h4>
                  Aucun clip trouvé. Vérifiez les critères de recherche.
                </Text>
              )}
            </View>
          )}
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  indicator: {
    flex: 1,
    justifyContent: 'center',
  },
  game: {
    width: '48%',
    marginBottom: 20,
    marginHorizontal: '1%',
    padding: 8,
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 1,
  },
  gameView: {
    alignItems: 'center',
  },
  gameText: {
    textAlign: 'center',
  },
});

export default ClipsScreen;
