import React, {useState, useEffect} from 'react';
import {
  FlatList,
  RefreshControl,
  StyleSheet,
  View,
  ActivityIndicator,
} from 'react-native';
import {getToken} from '../helpers/tokenHelper';
import {Picker} from '@react-native-picker/picker';
import Item from '../components/StreamerThumbnail';
import {theme, Input} from 'galio-framework';

import {readApi} from '../helpers/apiHelper';

const StreamersScreen = ({navigation}) => {
  async function getStreamers() {
    if (search !== '') {
      getStreamersByName();
      return;
    }
    isLoading(true);
    // let query = 'https://api.twitch.tv/helix/streams';
    let query = '/streams';
    if (lang !== '') {
      query += `?language=${lang}`;
      if (pagination !== '') {
        query += `&after=${pagination}&first=10`;
      }
    } else if (pagination !== '') {
      query += `?after=${pagination}&first=10`;
    }
    const token = await getToken();
    // fetch(query, {
    //   method: 'GET',
    //   headers: {
    //     'client-id': '4603kt3cahqmntrfyxd8y3g7liasqv',
    //     Authorization: `Bearer ${token}`,
    //   },
    // })
    readApi(query, 'GET')
      .then(response => response.json())
      .then(data => {
        let list = [];
        if (pagination !== '') {
          list = [...DATA];
        }
        setPagination(data.pagination.cursor);
        const promises = data.data.map(s =>
          // fetch(
          //   `https://api.twitch.tv/helix/search/channels?query=${s.user_name}&first=1`,
          //   {
          //     method: 'GET',
          //     headers: {
          //       'client-id': '4603kt3cahqmntrfyxd8y3g7liasqv',
          //       Authorization: `Bearer ${token}`,
          //     },
          //   },
          // ),
          readApi(`/search/channels?query=${s.user_name}&first=1`, 'GET'),
        );
        Promise.all(promises).then(values => {
          const channelsProm = values.map(v => v.json());
          Promise.all(channelsProm).then(channels => {
            data.data.forEach(s => {
              list.push({
                id: s.user_id,
                name: s.user_name,
                title: s.title,
                stream: s.thumbnail_url,
                thumbnail: channels.filter(c => c.data[0].id === s.user_id)[0]
                  .data[0].thumbnail_url,
                online: true,
              });
            });
            setData(list);
            isLoading(false);
            isRefreshing(false);
          });
        });
      })
      .catch(error => {
        isLoading(false);
        isRefreshing(false);
        console.log(error);
      });
  }

  async function getStreamersByName() {
    isLoading(true);
    // let query = `https://api.twitch.tv/helix/search/channels?query=${search}`;
    let query = `/search/channels?query=${search}`;
    if (pagination !== '') {
      query += `?after=${pagination}&first=10`;
    }
    const token = await getToken();
    // fetch(query, {
    //   method: 'GET',
    //   headers: {
    //     'client-id': '4603kt3cahqmntrfyxd8y3g7liasqv',
    //     Authorization: `Bearer ${token}`,
    //   },
    // })
    readApi(query, 'GET')
      .then(response => response.json())
      .then(data => {
        let list = [];
        if (pagination !== '') {
          list = [...DATA];
        }
        setPagination(data.pagination.cursor);
        data.data.forEach(s => {
          list.push({
            id: s.id,
            name: s.broadcaster_name,
            title: s.title,
            stream: s.thumbnail_url,
            thumbnail: s.thumbnail_url,
            online: s.is_live,
          });
        });
        setData(list);
        isLoading(false);
        isRefreshing(false);
      })
      .catch(error => {
        isLoading(false);
        isRefreshing(false);
        console.log(error);
      });
  }

  const renderItem = ({item}) => (
    <Item
      name={item.name}
      title={item.title}
      stream={item.stream}
      thumbnail={item.thumbnail}
      onPress={() =>
        navigation.navigate('StreamerDetail', {
          id: item.id,
          thumbnail: item.thumbnail,
          online: item.online,
        })
      }
    />
  );

  const renderFooter = () => {
    return (
      <View style={styles.indicator}>
        <ActivityIndicator size="large" color="black" />
      </View>
    );
  };

  const onRefresh = () => {
    setPagination('');
    isRefreshing(true);
  };

  const [lang, setLang] = useState('fr');
  const [DATA, setData] = useState([]);
  const [loading, isLoading] = useState(false);
  const [pagination, setPagination] = useState('');
  const [refreshing, isRefreshing] = useState(false);
  const [search, setSearch] = useState('');

  useEffect(() => {
    getStreamers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lang]);

  useEffect(() => {
    if (refreshing) {
      getStreamers();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [refreshing]);

  useEffect(() => {
    getStreamers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search]);

  return (
    <View style={styles.view}>
      <Input
        placeholder="Streamer"
        color={theme.COLORS.INFO}
        style={{borderColor: theme.COLORS.INFO}}
        placeholderTextColor={theme.COLORS.INFO}
        onChangeText={text => {
          setPagination('');
          setSearch(text);
        }}
        //TODO : détecter appui sur Entrée ou équivalent
      />
      {loading === true && pagination === '' ? (
        <View style={styles.indicator}>
          <ActivityIndicator size="large" color="black" />
        </View>
      ) : (
        <View style={styles.view}>
          <Picker
            selectedValue={lang}
            onValueChange={(itemValue, itemIndex) => {
              if (search === '') {
                setPagination('');
                setLang(itemValue);
              }
            }}>
            <Picker.Item label="Français" value="fr" />
            <Picker.Item label="English" value="en" />
            <Picker.Item label="Español" value="es" />
          </Picker>

          <FlatList
            data={DATA}
            renderItem={renderItem}
            keyExtractor={item => item.id}
            onEndReachedThreshold={0.4}
            onEndReached={getStreamers.bind(this)}
            ListFooterComponent={renderFooter.bind(this)}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh.bind(this)}
              />
            }
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  indicator: {
    flex: 1,
    justifyContent: 'center',
  },
});

export default StreamersScreen;
