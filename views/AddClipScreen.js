import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {getToken} from '../helpers/tokenHelper';
import {Input, theme} from 'galio-framework';

const AddClipScreen = ({navigation}) => {
  return (
    <View style={styles.Container}>
      <Input
        placeholder="ID du clip"
        color={theme.COLORS.THEME}
        style={{borderColor: theme.COLORS.THEME}}
        placeholderTextColor={theme.COLORS.THEME}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
});

export default AddClipScreen;
