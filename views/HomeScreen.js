import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Image} from 'react-native';
import MyButton from '../components/MyButton';
import {authorize} from 'react-native-app-auth';
import LoginForm from '../components/LoginForm';

import Icon from 'react-native-vector-icons/AntDesign';
import {Button} from 'galio-framework';

import {setToken as setTok} from '../helpers/tokenHelper';
import {BACKEND, CLIENT_ID} from '../helpers/serverAccessHelper';

const config = {
  redirectUrl: 'webapk://callback',
  clientId: CLIENT_ID,
  usePKCE: true,
  scopes: ['api'],
  dangerouslyAllowInsecureHttpRequests: BACKEND.match(/^http:\/\/.*/) !== null,
  additionalHeaders: {Accept: 'application/json'},
  serviceConfiguration: {
    authorizationEndpoint: `${BACKEND}/oauth/authorize`,
    tokenEndpoint: `${BACKEND}/oauth/token`,
  },
};

const HomeScreen = ({navigation}) => {
  async function connectWithTwitch() {
    console.log('authorize');
    try {
      const result = await authorize(config);
      console.log('result', result);
      setToken(result.accessToken);
      setTok(result.accessToken);
    } catch (err) {
      console.log(err);
    }
  }

  function connect(username, password) {
    console.log('login', username, password);
    fetch(`${BACKEND}/login`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: `${JSON.stringify({login: username, password})}`,
    })
      .then(response => response.json())
      .then(data => {
        console.log(data);
        console.log(data.token);
        if (data.token) {
          setToken(data.token);
          setTok(data.token);
        }
      })
      .catch(error => console.log(error));
  }

  function readAPI() {
    fetch(`${BACKEND}/public_api/proxy/search/channels?query=a_seagull`, {
      method: 'GET',
      headers: {'Content-Type': 'application/json'},
    })
      .then(response => response.text())
      .then(data => {
        console.log(data);
        setSeagullURI(JSON.parse(data).data[0].thumbnail_url);
      })
      .catch(error => console.log(error));
  }

  function askUser() {
    fetch(`${BACKEND}/api/user`, {
      method: 'GET',
      headers: {'Content-Type': 'application/json', authorization: token},
    })
      .then(response => response.json())
      .then(data => console.log(data))
      .catch(error => console.log(error));
  }

  const [token, setToken] = useState('');
  const [isAdmin, setIsAdmin] = useState(false);
  const [seagullURI, setSeagullURI] = useState(null);

  useEffect(() => {
    if (token === '') {
      navigation.setOptions({
        headerRight: () => null,
      });
    } else {
      navigation.setOptions({
        headerRight: () => (
          <View>
            <Button
              color="#ff7615"
              style={{width: 40, height: 40}}
              onPress={() => {
                setToken('');
                setTok('');
                //setSeagullURI(null);
              }}>
              <Icon name="poweroff" color="white" size={25} />
            </Button>
          </View>
        ),
      });
    }

    if (token === '' || token === 'public') {
      setIsAdmin(false);
      return;
    }

    console.log(token);
    fetch(`${BACKEND}/api/isAdmin`, {
      method: 'GET',
      headers: {'Content-Type': 'application/json', authorization: token},
    })
      .then(response => response.json())
      .then(data => {
        console.log(data);
        if (data.role) {
          setIsAdmin(data.role);
        }
      })
      .catch(err => console.log('admin', err));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);
  return (
    <View>
      {token === '' ? (
        <View>
          <LoginForm testID="logform" onConnect={connect} />
          <MyButton
            title="Se connecter avec Backend et Twitch"
            onPress={connectWithTwitch}
          />
          <MyButton
            title="Continuer sans se connecter"
            onPress={() => {
              setToken('public');
              setTok('public');
            }}
          />
        </View>
      ) : (
        <View>
          <MyButton
            title="Deconnecter"
            onPress={() => {
              setToken('');
              setTok('');
              setSeagullURI(null);
            }}
          />
          {/* <MyButton title="know username" onPress={askUser} /> */}
          <MyButton
            title="Streamers"
            onPress={() => navigation.navigate('Streamers')}
          />
          <MyButton
            title="Clips"
            onPress={() => navigation.navigate('Clips')}
          />
          {isAdmin ? (
            <View>
              <MyButton title="Seagull !!" onPress={readAPI} />
              <MyButton
                title="Espace admin"
                onPress={() => navigation.navigate('Admin')}
              />
              <Image
                testID="seagull"
                source={{uri: seagullURI}}
                style={styles.imageStyle}
              />
            </View>
          ) : null}
        </View>
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  imageStyle: {
    width: 200,
    height: 300,
    resizeMode: 'center',
  },
});
export default HomeScreen;
