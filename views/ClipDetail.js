import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import ClipWebView from '../components/ClipWebView';
import MyButton from '../components/MyButton';
import {getToken, isConnected} from '../helpers/tokenHelper';

import {BACKEND} from '../helpers/serverAccessHelper';

const ClipsScreen = ({navigation, route}) => {
  const url = route.params.url;
  const id = route.params.id;

  function like() {
    const query = !voted
      ? `${BACKEND}/api/clips/${id}/upvote`
      : `${BACKEND}/api/clips/${id}/downvote`;

    fetch(query, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        authorization: getToken(),
      },
    })
      .then(response => {
        console.log(response);
        response.json();
      })
      .then(data => {
        console.log(data);
        loadScore(); // reset score for searching value once again
        checkVoted();
      })
      .catch(err => console.log(err));
  }

  function loadScore() {
    fetch(`${BACKEND}/public_api/clips/${id}/score`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(data => {
        if (!data.score) {
          setScore('Not liked yet');
        } else {
          setScore(`${data.score} like${data.score > 1 ? 's' : ''}`);
        }
      })
      .catch(err => console.log(err));
  }

  function checkVoted() {
    fetch(`${BACKEND}/api/clips/${id}/checkvoted`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        authorization: getToken(),
      },
    })
      .then(response => response.json())
      .then(data => {
        console.log(data);
        setVoted(data.voted);
      })
      .catch(err => console.log(err));
  }

  useEffect(() => {
    loadScore();
    checkVoted();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [score]);

  const [score, setScore] = useState(0);
  const [voted, setVoted] = useState(false);

  return (
    <View style={styles.Container}>
      <ClipWebView urlClip={url} />
      <Text style={styles.Text}>{score}</Text>
      {isConnected() ? (
        <MyButton title={`${voted ? 'unlike' : 'like'}`} onPress={like} />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },

  Text: {
    textAlign: 'center',
    fontSize: 30,
  },
});

export default ClipsScreen;
