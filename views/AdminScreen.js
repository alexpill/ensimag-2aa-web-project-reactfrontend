import React, {useState, useEffect} from 'react';
import {StyleSheet, View, ActivityIndicator, FlatList} from 'react-native';
import {getToken} from '../helpers/tokenHelper';
import {theme, Input, Button, Text} from 'galio-framework';
import Icon from 'react-native-vector-icons/AntDesign';

import {BACKEND} from '../helpers/serverAccessHelper';

const AdminScreen = ({navigation}) => {
  async function getUsers() {
    const query = `${BACKEND}/api/admin/users`;
    fetch(query, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        authorization: getToken(),
      },
    })
      .then(response => response.json())
      .then(data => {
        let list = [];
        if (data.data) {
          data.data.forEach((u, i) => list.push({id: i, username: u.username}));
        }
        setData(list);
        isLoading(false);
      })
      .catch(error => {
        isLoading(false);
        console.log(error);
      });
  }

  function addUser() {
    const query = `${BACKEND}/api/admin/users/${username}/${password}`;
    fetch(query, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        authorization: getToken(),
      },
    })
      .then(response => response.json())
      .then(data => {
        alert(data.msg ?? data.error);
        getUsers();
      })
      .catch(error => {
        console.log(error);
        alert(error);
      });
  }

  function removeUser(item) {
    const query = `${BACKEND}/api/admin/users/${item.username}`;
    fetch(query, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        authorization: getToken(),
      },
    })
      .then(response => response.json())
      .then(data => {
        alert(data.msg ?? data.error);
        setUsername('');
        setPassword('');
        getUsers();
      })
      .catch(error => {
        console.log(error);
        alert(error);
      });
  }

  const renderItem = ({item}) => (
    <View style={styles.item}>
      <Text p>{item.username}</Text>
      <Button
        color="#ff7615"
        style={{width: 20, height: 20}}
        onPress={() => {
          removeUser(item);
        }}>
        <Icon name="minuscircleo" color="white" size={15} />
      </Button>
    </View>
  );

  const [DATA, setData] = useState([]);
  const [loading, isLoading] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    getUsers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.Container}>
      {loading === true ? (
        <View style={styles.indicator}>
          <ActivityIndicator size="large" color="black" />
        </View>
      ) : (
        <View style={styles.Container}>
          {DATA === [] ? (
            <Text h4>Aucun utilisateur.</Text>
          ) : (
            <FlatList
              data={DATA}
              renderItem={renderItem}
              keyExtractor={item => item.id}
            />
          )}
          <View style={{alignItems: 'center'}}>
            <Input
              placeholder="Nom d'utilisateur"
              color={theme.COLORS.INFO}
              style={{borderColor: theme.COLORS.INFO, width: '90%'}}
              placeholderTextColor={theme.COLORS.INFO}
              onChangeText={setUsername}
              value={username}
            />
            <Input
              placeholder="Mot de passe"
              password
              color={theme.COLORS.INFO}
              style={{borderColor: theme.COLORS.INFO, width: '90%'}}
              placeholderTextColor={theme.COLORS.INFO}
              onChangeText={setPassword}
              value={password}
            />
            <Button onPress={addUser} color="warning" style={styles.addUserBtn}>
              Ajouter un utilisateur
            </Button>
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 5,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
  },
  indicator: {
    flex: 1,
    justifyContent: 'center',
  },
  addUserBtn: {
    width: 200,
    height: 40,
  },
});

export default AdminScreen;
