// import {setToken, getToken} from './tokenHelper';
import {BACKEND} from './serverAccessHelper';

export function readApi(url, method) {
  return fetch(`${BACKEND}/public_api/proxy${url}`, {
    method: method,
    headers: {
      'Content-Type': 'application/json' /*, authorization: getToken()*/,
    },
  });
}
