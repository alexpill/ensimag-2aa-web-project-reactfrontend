describe('HomeScreen aka first page test', () => {
    beforeAll(async () => {
        await device.launchApp();
    });

    beforeEach(async () => {
        await device.reloadReactNative();
    });

    it('should have connection screen', async () => {
        await expect(element(by.text('Se connecter'))).toBeVisible();
        await expect(element(by.text('Se connecter avec Backend et Twitch'))).toBeVisible();
        await expect(element(by.text('Continuer sans se connecter'))).toBeVisible();
        await expect(element(by.text('alzebor'))).toBeVisible();
    });

    it("should show another screen after clicking on 'Se connecter' with admin account", async () => {
        await element(by.text('Se connecter')).tap();
        await expect(element(by.text('Deconnecter'))).toBeVisible();
        await expect(element(by.text('Streamers'))).toBeVisible();
        await expect(element(by.text('Clips'))).toBeVisible();
        await expect(element(by.text('Seagull !!'))).toBeVisible();
        await expect(element(by.text('Espace admin'))).toBeVisible();
        await expect(element(by.id('seagull'))).toExist();
    });

    it("should show another screen after clicking 'Se connecter' with not admin account", async () => {
        // above do not work because the custom component do not set testID props
        // await element(by.id('logform')).replaceText('enki')
        await element(by.text('alzebor')).replaceText('enki')
        await element(by.text('Se connecter')).tap();
        await expect(element(by.text('Deconnecter'))).toBeVisible();
        await expect(element(by.text('Streamers'))).toBeVisible();
        await expect(element(by.text('Clips'))).toBeVisible();
        await expect(element(by.text('Seagull !!'))).toNotExist();
        await expect(element(by.text('Espace admin'))).toNotExist();
        await expect(element(by.id('seagull'))).toNotExist();
    });

    it("should show another screen after connecting as visitor", async () => {
        await element(by.text('Continuer sans se connecter')).tap();
        await expect(element(by.text('Deconnecter'))).toBeVisible();
        await expect(element(by.text('Streamers'))).toBeVisible();
        await expect(element(by.text('Clips'))).toBeVisible();
        await expect(element(by.text('Seagull !!'))).toNotExist();
        await expect(element(by.text('Espace admin'))).toNotExist();
        await expect(element(by.id('seagull'))).toNotExist();
    });

});