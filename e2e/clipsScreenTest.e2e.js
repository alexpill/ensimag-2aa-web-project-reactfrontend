describe('ClipsScreen test as visitor', () => {
    beforeAll(async () => {
        await device.launchApp();
    });

    beforeEach(async () => {
        await device.reloadReactNative();
        await element(by.text('Continuer sans se connecter')).tap();
        await element(by.text('Clips')).tap();
    });

    it('should present severals clip categories', async () =>{
        await expect(element(by.text('Clips'))).toBeVisible();
        await expect(element(by.text('Streamer'))).toBeVisible();
        // await expect(element(by.text('Recherche...'))).toBeVisible();
        // TODO: fix below comments (use mocking)
        // Below are some categories... be warn that they might change... that's
        // why we need to have mocking and not direct testing
        await expect(element(by.text('Just Chatting'))).toBeVisible();
        await expect(element(by.text('Grand Theft Auto V'))).toBeVisible();
        await expect(element(by.text('League of Legends'))).toBeVisible();
    })

    it('should change screen clicking on a cat', async () => {
        // clicking on one cat
        await element(by.text('Just Chatting')).tap();
        await expect(element(by.text('Jeu'))).toBeVisible();
        await expect(element(by.text('Retour aux jeux'))).toBeVisible();
    })

    it('should allow to change way of sorting clips in one cat', async () =>{
        await element(by.text('Just Chatting')).tap();
        // changing 'Jeu' for 'Id'
        await element(by.text('Jeu')).tap();
        await expect(element(by.text('Id'))).toBeVisible();
        await element(by.text('Id')).tap();
        await expect(element(by.text('Id'))).toBeVisible();
    })

    it('should allow to get back to categories after clicking one specific cat', async () => {
        await element(by.text('Just Chatting')).tap();
        // getting back to categories
        await element(by.text('Retour aux jeux')).tap();
        await expect(element(by.text('Streamer'))).toBeVisible();
    })

})