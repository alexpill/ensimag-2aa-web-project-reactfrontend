import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './views/HomeScreen';
import ClipsScreen from './views/ClipsScreen';
import ClipDetail from './views/ClipDetail';
import AddClip from './views/AddClipScreen';
import StreamersScreen from './views/StreamersScreen';
import StreamerDetail from './views/StreamerDetail';
import AdminScreen from './views/AdminScreen';

const App = () => {
  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerTintColor: 'white',
          headerStyle: {backgroundColor: 'mediumturquoise'},
          headerTitleStyle: {
            color: 'midnightblue',
          },
        }}>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{title: 'La Plymobil'}}
        />
        <Stack.Screen
          name="Clips"
          component={ClipsScreen}
          options={{title: 'Clips'}}
        />
        <Stack.Screen
          name="ClipDetail"
          component={ClipDetail}
          options={{title: 'Clip'}}
        />
        <Stack.Screen
          name="AddClip"
          component={AddClip}
          options={{title: 'Ajout'}}
        />
        <Stack.Screen
          name="Streamers"
          component={StreamersScreen}
          options={{title: 'Streamers'}}
        />
        <Stack.Screen
          name="StreamerDetail"
          component={StreamerDetail}
          options={{title: 'Streamer'}}
        />
        <Stack.Screen
          name="Admin"
          component={AdminScreen}
          options={{title: 'Utilisateurs'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
